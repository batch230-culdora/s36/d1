const express = require("express");
// express.Router() method allows access to HTTP methods
const router = express.Router();

const taskControllers = require("../controllers/taskController");

// Create - task routes
router.post("/addTask", taskControllers.createTaskController);
// Get all tasks
router.get("/allTask", taskControllers.getAllTaskController);
// Delete all tasks by ID
router.delete("/deleteTask/:taskId", taskControllers.deleteTaskController);

router.put("/archive/:id", (req, res) => {
	taskControllers.changeNameToArchive(req.params.id).then(resultFromController => res.send(resultFromController));
})

router.patch("/updateTask/:taskId", taskControllers.updateTaskNameController);

// ACTIVITY----------------------s36



router.get("/specificTask/:taskId", taskControllers.getSpecificTaskController);


router.put("/complete/:id", (req, res) => {
	taskControllers.changeStatusToComplete(req.params.id).then(resultFromController => res.send(resultFromController));
})

module.exports = router;